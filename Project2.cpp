﻿#include <cstdlib>
#include <iostream>
#include <ctime>
#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

// variables that show how many rooms are left
unsigned int freeBy200 = 10;
unsigned int freeBy400 = 10;
unsigned int freeBy600 = 5;
// how many people didn't find any room
unsigned int peopleLeft = 0;

// time from the program start
unsigned int startTime;

// mutex for finishing condition variable
mutex finishLock;
// mutex for cout
mutex printLock;
// condition variable
condition_variable finished;

/// <summary>
/// parallel thread function
/// </summary>
void Customers()
{
	// initialization of random numbers generator
	srand(static_cast<unsigned int>(time(0)));
	// order number of a customer
	unsigned long ind = 0;
	while (true)
	{
		// check if hotel is full
		if (freeBy200 + freeBy400 + freeBy600 <= 0)
		{
			// giving control to the main thread
			unique_lock<mutex> lock(finishLock);
			finished.notify_one();
			printLock.lock();
			cout << "Time: " << (clock() - startTime) << ". There are no rooms available anymore.\n";
			printLock.unlock();
			return;
		}
		// find a room that customer can afford or increment number of people left if couldn't find any
		unsigned long money = rand() % 800;
		cout << "\nTime: " << (clock() - startTime) << ". Customer #" << ind << " entered hotel with " << money << " rubles.\n";
		cout << "Available: " << freeBy200 << " rooms by 200, " << freeBy400 << " rooms by 400 and " << freeBy600 << " rooms by 600.\n";
		if (money >= 600 && freeBy600 > 0)
		{
			freeBy600--;
			cout << "Time: " << (clock() - startTime) << ". Customer #" << ind << " took a room by 600.\n";
			cout << "Available now: " << freeBy600 << " rooms by 600 and " << (freeBy200 + freeBy400 + freeBy600) << " rooms overall.\n";
		}
		else if (money >= 400 && freeBy400 > 0)
		{
			freeBy400--;
			cout << "Time: " << (clock() - startTime) << ". Customer #" << ind << " took a room by 400.\n";
			cout << "Available now: " << freeBy400 << " rooms by 400 and " << (freeBy200 + freeBy400 + freeBy600) << " rooms overall.\n";
		}
		else if (money >= 200 && freeBy200 > 0)
		{
			freeBy200--;
			cout << "Time: " << (clock() - startTime) << ". Customer #" << ind << " took a room by 200.\n";
			cout << "Available now: " << freeBy200 << " rooms by 200 and " << (freeBy200 + freeBy400 + freeBy600) << " rooms overall.\n";
		}
		else
		{
			peopleLeft++;
			cout << "Time: " << (clock() - startTime) << ". Customer #" << ind << " couldn't find a free room he could afford and left the hotel.\n";
		}
		cout << '\n';
		ind++;
	}
}

int main(int argc, char* argv[])
{
	startTime = clock();
	cout << "Time: " << (clock() - startTime) << ". Hotel is open now.\n";

	// making the parallel thread
	thread hotelThread(Customers);
	unique_lock<mutex> locker(finishLock);

	// waiting for signal from the parallel thread through the condition variable
	finished.wait(locker, [&]() {return (freeBy200 + freeBy400 + freeBy600 <= 0); });

	printLock.lock();
	cout << "Number of people that couldn't find the room: " << peopleLeft << '\n';
	printLock.unlock();
	// waiting for parallel thread to finish
	hotelThread.join();
}